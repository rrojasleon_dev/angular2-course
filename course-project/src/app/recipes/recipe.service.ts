import { Injectable } from '@angular/core';

import { Recipe } from "./recipe.model";
import { Ingredient } from '../shared/ingredient.model';
import { ShoppingListService } from '../shopping-list/shpping-list.service';

@Injectable()
export class RecipeService {
    
    private recipes: Recipe[] = [
        new Recipe(
            'A Steak', 
            'Da\' steak', 
            'https://c.pxhere.com/photos/4b/79/steak_delicious_food_dinner_grill_grilled_steak_dinner_lunch-608139.jpg!d',
            [
                new Ingredient('Meat', 1),
                new Ingredient('oil', 1)
            ]
        ),
        new Recipe(
            'Sushi', 
            'Some fine rice', 
            'https://images.pexels.com/photos/327172/pexels-photo-327172.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260',
            [
                new Ingredient('Rice', 2),
                new Ingredient('chicken', 1)
            ]
        )
    ];

    constructor(private slService: ShoppingListService) {}

    getRecipes() {
        return this.recipes.slice();
    }

    getRecipe(index: number) {
        return this.recipes[index];
    }
    addIngredientsToShoppingList(ingredients: Ingredient[]) {
        this.slService.addIngredients(ingredients); 
    }
}